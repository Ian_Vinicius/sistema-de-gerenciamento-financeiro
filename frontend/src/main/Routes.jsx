import React from "react";
import { Routes, Route } from 'react-router-dom'

import Main from "../components/template/Main";

//Ajustar componentes com localStorage
import AboutG from "../components/servicos/AboutG";
import AboutRel from "../components/servicos/AboutRel";
import AboutD from "../components/servicos/AboutD";
import AboutR from "../components/servicos/AboutR";
import Home from "../components/sistema/Home";
import AboutC from "../components/cadastro/Cadastro";

import Despesas from '../components/sistema/areas/Despesas';
import Limites from '../components/sistema/areas/Limites';
import Relatorio from '../components/sistema/areas/Relatorio';

import Recuperar from "../components/cadastro/Recuperar";

const SystemRoutes = () =>{
    return(
        // Ajustar telas através de login de usuário
        <Routes>
            <Route exact path="/" element={<Main />}/>
            <Route path="*" element={<Main />}/>

            {/* Before user login */}
            <Route path="/servico/limite-gastos" element={<AboutG />}/>
            <Route path="/servico/relatorio" element={<AboutRel />}/>
            <Route path="/servico/despesas" element={<AboutD />}/>
            <Route path="/servico/receitas" element={<AboutR />}/>
            <Route path="/cadastro" element={<AboutC />}/>

            {/* After user login */}
            <Route path="/home" element={<Home />}/>
            <Route path="/despesas" element={<Despesas />}/>
            <Route path="/limites" element={<Limites />}/>
            <Route path="/relatorio" element={<Relatorio />}/>

        </Routes>
    )
}

export default SystemRoutes;