import React from "react";
import axios from "axios";
import "./Cadastro.css"

import { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { useUserContext } from '../../UserContext';

const Cadastro = () => {
    const navigate = useNavigate();
    const { setUserId } = useUserContext();

    const [isloggedIn, setisLoggedIn] = useState(null);
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleUsernameChange = (event) => {
      setUsername(event.target.value);
    };

    const handleEmailChange = (event) => {
      setEmail(event.target.value);
    };

    const handlePassWordChange = (event) => {
      setPassword(event.target.value);
    };

    const [logEmail, verifyEmail] = useState('');
    const [logPassword, verifyPassword] = useState('');

    const handleLogEmailChange = (event) => {
      verifyEmail(event.target.value);
    };

    const handleLogPassWordChange = (event) => {
      verifyPassword(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
          const response = await axios.post("http://localhost:5000/register", {
            username,
            email,
            password,
          });
          console.log(response.data);
          setUsername("");
          setEmail("");
          setPassword("");
        } catch (error) {
          console.log(error);
        }
      };

    const userLogin = async (event) => {
      event.preventDefault();
      try{
        const response = await axios.post("http://localhost:5000/login", { 
          logEmail, 
          logPassword 
        }, { withCredentials: true }); 
        console.log(response.data)
        verifyEmail("");
        verifyPassword("");
        setisLoggedIn(true)

        if (response.data.success) {
          const userId = response.data.idusuario;
          setUserId(userId)
          navigate('/home');
        } else {
          console.log("Não foi possível ir à home.");
        }
      } catch (error) {
        console.log(error);
      }
    };
    
    return(
        <div className="principal">
            <section className="leftzone">
                <a className="logo" href="/">Icone</a>
                <form className="cadastrar" onSubmit={handleSubmit}>
                    <center><h1 id="titulo-cadastrar">Cadastrar conta</h1></center>
                    <input type="text" value={username} onChange={handleUsernameChange}  placeholder="Nome"/>
                    <input type="email"  value={email} onChange={handleEmailChange} placeholder="Email"/>
                    <input type="password" value={password} onChange={handlePassWordChange}  placeholder="Senha"/>
                    <a className="botao-cadastrar"><button>Cadastrar</button></a>
                </form>

                <p id="aurum">Aurum</p>
                <p id="direitos">© Todos os Direitos reservados</p>
            </section>
        
            <section className="rightzone">
                <form className="entrar" onSubmit={userLogin}>
                    <center><h1 id="titulo-entrar">Entrar na conta</h1></center>
                    <input type="email" value={logEmail} onChange={handleLogEmailChange} placeholder="Email"/>
                    <input type="password" value={logPassword} onChange={handleLogPassWordChange} placeholder="Senha"/>
                    <a><button className="botao-entrar">Entrar</button></a>
                </form>

                <a id="esqueci-senha" href="/recuperar">Esqueceu sua senha?</a>
            </section>
        </div>
    )
}
export default Cadastro;