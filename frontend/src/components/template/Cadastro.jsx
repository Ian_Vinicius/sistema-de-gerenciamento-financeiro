import React, { Component, Fragment, useState, useEffect } from "react";
import axios from "axios";

import { Link, redirect, useNavigate } from "react-router-dom";


import Home from "../sistema/Home";
import "./Cadastro.css"

const Cadastro = () =>{
    const navigate = useNavigate();
    const [isloggedIn, setisLoggedIn] = useState(null);

    const logOut = () => {
        setisLoggedIn(false)    
    };

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [logEmail, verifyEmail] = useState("");
    const [logPassword, verifyPassword] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
          const response = await axios.post("http://localhost:5000/register", {
            username,
            email,
            password,
          });
          console.log(response.data);
          setUsername("");
          setEmail("");
          setPassword("");
        } catch (error) {
          console.log(error);
        }
      };

      // vasconcelos.iansantos@gmail.com
    const userLogin = async (event) => {
      event.preventDefault();
      try{
        const response = await axios.post("http://localhost:5000/login", { 
          logEmail, 
          logPassword 
        });
        console.log(response.data)
        verifyEmail("");
        verifyPassword("");
        setisLoggedIn(true)

        if(response.data.success){
          navigate('/home');
        } else{ console.log("Não foi possível ir à home.") }

      } catch (error){ console.log(error) }
    };


    return(

          <Fragment>
              <div className="principal">
                  <section className="leftzone">
                      <h1 className="logo">ICONE</h1>
                      <form className="cadastrar" onSubmit={handleSubmit}>
                          <center><h1 id="titulo-cadastrar">Cadastrar conta</h1></center>
                          <input type="text" value={username} onChange={(event) => setUsername(event.target.value)} placeholder="Nome"/>
                          <input type="email" id="email" value={email} onChange={(event) => setEmail(event.target.value)} placeholder="Email"/>
                          <input type="password" value={password} onChange={(event) => setPassword(event.target.value)} id="password" placeholder="Senha"/>
                          <a className="botao"><button className="botao-cadastrar">Cadastrar</button></a>
                      </form>
                  </section>

                  <section className="rightzone">
                    <form className="entrar" onSubmit={userLogin}>
                        <center><h1 id="teste">Entrar na conta</h1></center>
                        <input type="email" value={logEmail} onChange={(event) => verifyEmail(event.target.value)} placeholder="Email"/>
                        <input type="password" value={logPassword} onChange={(event) => verifyPassword(event.target.value)} placeholder="Senha"/>
                        <center><a><button className="botao-entrar">Entrar</button></a></center>
                    </form>
                  </section>
              </div>
          </Fragment>
        )
    }

export default Cadastro;