import React from "react";
import './Nav.css';

const Nav = props => {
    return(
        <aside className="nav d-none d-sm-flex" style={props.style}>
            <nav className="nav-items">   
                <li><a href="/inicial" style={props.nav}>Início</a></li>
                <li><a href="#sobre" style={props.nav}>Sobre</a></li>
                <li><a href="#servicos" style={props.nav}>Serviços</a></li>
                <li><a id="nosso-time" href="#equipe" style={props.border}>Nosso time</a></li>
                <li><a href="/cadastro"><button style={props.backg}>Começar</button></a></li>
            </nav>
        </aside>
    )
};

export default Nav;
    