import React from "react";
import './HomeNav.css';

export default function HomeNav () {
    return (
        <div className="head-nav">
                <aside className="nav d-none d-sm-flex">
                    <nav className="nav-items">   
                        <li><a href="/home">Visão geral</a></li>
                        <li><a href="/despesas">Despesas</a></li>
                        <li><a href="/relatorio">Relatórios</a></li>
                        <li><a href="/limites">Limites de gastos</a></li>
                    </nav>
                </aside>
        </div>
    )
};