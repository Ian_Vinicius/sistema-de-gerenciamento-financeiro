import React, { Fragment, useState } from "react";
import './Formulario.css';
import axios from "axios";

const Formulario = (props) => {

  const [userID, setUserID] = useState('');
  const [description, setDescription] = useState('');
  const [type, setType] = useState('');
  const [value, setValue] = useState('');
  const [date, setDate] = useState('');

  const [selectedOption, setSelectedOption] = useState('');

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleValueChange = (event) => {
    setValue(event.target.value);
  };

  const handleDateChange = (event) => {
    setDate(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
        const response = await axios.post(`http://localhost:5000/${props.URL}`, { 
            userID: localStorage.getItem('userId'),
            description, 
            type: selectedOption, 
            value, 
            date, 
        });
        console.log(response.data);
        setUserID("");
        setDescription("");
        setType("");
        setValue("");
        setDate("");
    } catch (error) {
        console.log(error);
    };
  };

  return (
    <Fragment>
      <h2 style={props.style}>{props.Titulo}</h2>
      <form className="formulario" onSubmit={handleSubmit}>
        <div className='forms-area'>
            <label htmlFor="descricao">Título:</label>
            <input type="text" id="descricao" name="descricao" value={props.description} onChange={handleDescriptionChange}/>
            
            <label htmlFor="valor">Valor:</label>
            <input type="number" id="valor" name="valor" value={props.value} onChange={handleValueChange}/>
        </div>

        <div className='forms-area'>
            <label htmlFor="tipo">Tipo:</label>
            <select id="tipo" name="tipo" value={props.selectedOption} onChange={(e) => setSelectedOption(e.target.value)}>
              <option value={props.Option1}>{props.Option1}</option>
              <option value={props.Option2}>{props.Option2}</option>
              <option value={props.Option3}>{props.Option3}</option>
              <option value={props.Option4}>{props.Option4}</option>
              <option value={props.Option5}>{props.Option5}</option>
            </select>
          
            <label htmlFor="data">Data:</label>
            <input type="date" id="data" name="data" value={props.date} onChange={handleDateChange}/>
            <a><button className="botao-enviar">Enviar</button></a>
        </div>
            {/* <input type="submit" value="Enviar"/> */}
        </form>
    </Fragment>
  );
};

export default Formulario;