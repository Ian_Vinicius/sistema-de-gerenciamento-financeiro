import React, { Fragment } from "react";
import '../services/styles/UserDash.css';
import Formulario from "../../template/Formulario";
import Offcanvas from "./Offcanvas";

import buttonRevenue from "../../../assets/img/plus.png";
import buttonSpent from "../../../assets/img/error.png";

const UserDash = (props) => {
    return(
        <Fragment>
            <div className="user-dash-area">
                <h1 key={1} style={{color: '#000000', fontWeight: '800', fontSize:"1.8rem"}}>{props.Nome}</h1>
                <p key={2} style={{borderLeft:"2px solid #e0e0e0"}}>Saldo<br></br><a style={{color: '#000000', fontWeight: '800', fontSize:"1.3rem"}}>R${props.Saldo}</a></p>

                <Offcanvas ButtonName='Receita' IMG={buttonRevenue} style={{color:'#00c53b', border:'1px solid#ecebebb5', borderRadius:"5px", width:"25%", cursor:"pointer"}}>
                    <Formulario URL={'revenue'} Titulo="Novo recebimento" style={{color:'#00c53b', fontWeight:"700"}} Option1="Salário" Option2="Auxílio" Option3="Empréstimo" Option4="Investimentos" Option5="Outros"/>
                </Offcanvas>
                <Offcanvas ButtonName='Despesa' IMG={buttonSpent} style={{color:'#ff0000', border:'1px solid#ecebebb5', borderRadius:"5px", width:"25%", cursor:"pointer"}}>
                    <Formulario URL={'spent'} Titulo="Nova despesa" style={{color:'#ff0000', fontWeight:"700"}} Option1="Saúde" Option2="Alimentação" Option3="Transporte" Option4="Investimentos" Option5="Outros"/>
                </Offcanvas>
                
                <span className="dash-relatorio-button">Relatórios</span>

                <p key={3}>Receita mensal<br></br><a style={{color: '#00c53b', fontWeight: '800'}}>R$0</a></p>
                <p key={4}>Despesa mensal<br></br><a style={{color: '#ff0000', fontWeight: '800'}}>R$0</a></p>
            </div>
        </Fragment>
    )
};

export default UserDash;
