import React, { Fragment, useState } from "react";
import axios from "axios";
import './styles/UserSpent.css';
import Offcanvas from "./Offcanvas";
import Formulario from "../../template/Formulario";

import pencilIcon from "../../../assets/img/pencil-fill.svg";
import trashIcon from "../../../assets/img/trash3-fill.svg";

export default function UserSpent(props) {
    
    function dateFormat(data) {
        const dataObj = typeof data === 'string' ? new Date(data) : data;
      
        const dia = dataObj.getDate().toString().padStart(2, '0');
        const mes = (dataObj.getMonth() + 1).toString().padStart(2, '0');
        const ano = dataObj.getFullYear();
      
        return `${dia}/${mes}/${ano}`;
      }
      
      function formatarData(received) {
        const date = new Date(received)
        const dia = date.getDate().toString().padStart(2, '0');
        const mes = (date.getMonth() + 1).toString().padStart(2, '0');
        const ano = date.getFullYear();
        return `${ano}-${mes}-${dia}`;
      }      

      const [userID, setUserID] = useState('');
      const [description, setDescription] = useState('');
      const [type, setType] = useState('');
      const [value, setValue] = useState('');
      const [date, setDate] = useState('');
    
      const [selectedOption, setSelectedOption] = useState('');
    

      const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
      };
    
      const handleValueChange = (event) => {
        setValue(event.target.value);
      };
    
      const handleDateChange = (event) => {
        setDate(event.target.value);
      };

      const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.post(`http://localhost:5000/spent/update}`, { 
                userID: localStorage.getItem('userId'),
                description, 
                type: selectedOption, 
                value, 
                date, 
            });
            console.log(response.data);
            setUserID("");
            setDescription("");
            setType("");
            setValue("");
            setDate("");
        } catch (error) {
            console.log(error);
        };
      };

      const handleDeleteSpent = async (event) => {
        console.log('Hi there!')
      };

    return (
        <Fragment>
        <div className="user-spent-area">
        <h4 style={{fontWeight:"700"}}>Despesas</h4>
            {props.spentContent.users.map((user, userIndex) => (
                user.despesas.map((despesa, despesaIndex) => (
                    <div key={`user-${userIndex}-despesa-${despesaIndex}`} className="d-spent">
                        <div style={{marginTop:"0.9rem"}}>
                            <p className="spent-name">{despesa[0]}</p>
                            <p className="spent-date" style={{fontSize: "0.9rem", marginBottom:"0rem"}}>{dateFormat(despesa[2])}</p>
                        </div>
                        <div style={{marginTop:"0.9rem"}}>
                            <p className="spent-value" style={{color: "#ff0000", fontWeight: "800", marginBottom: "0rem"}}>R${despesa[1]}</p>
                            <p className="spent-type" style={{fontSize: "0.9rem"}}>{despesa[3]}</p>
                        </div>
                            <Offcanvas IMG={pencilIcon} style={{marginLeft:"0rem", marginTop:"0rem"}}>
                                {/* <Formulario URL={'revenue/update'} Titulo="Atualizar recebimento" style={{color:'#00c53b', fontWeight:"700"}} Option1="Salário" Option2="Auxílio" Option3="Empréstimo" Option4="Investimentos" Option5="Outros" description={despesa[0]} value={despesa[1]} date={formatarData(despesa[2])} selectedOption={despesa[3]}  /> */}
                                
                                <h2 style={{color:'#ff0000', fontWeight:"700"}}>Atualizar: {despesa[0]}</h2>
                                <p>Valor: {despesa[1]}</p>
                                <p>Data: {despesa[2]}</p>
                                <p>Tipo: {despesa[3]}</p>

                                <form className="formulario" onSubmit={handleSubmit}>
                                    <div className='forms-area'>
                                        <label htmlFor="descricao">Título:</label>
                                        <input type="text" id="descricao" name="descricao" onChange={handleDescriptionChange}/>
                                        
                                        <label htmlFor="valor">Valor:</label>
                                        <input type="number" id="valor" name="valor" onChange={handleValueChange}/>
                                    </div>

                                    <div className='forms-area'>
                                        <label htmlFor="tipo">Tipo:</label>
                                        <select id="tipo" name="tipo" onChange={(e) => setSelectedOption(e.target.value)}>
                                        <option value={'Saúde'}>{'Saúde'}</option>
                                        <option value={'Transporte'}>{'Transporte'}</option>
                                        <option value={'Alimentação'}>{'Alimentação'}</option>
                                        <option value={'Investimentos'}>{'Investimentos'}</option>
                                        <option value={'Outros'}>{'Outros'}</option>
                                        </select>
                                    
                                        <label htmlFor="data">Data:</label>
                                        <input type="date" id="data" name="data" onChange={handleDateChange}/>
                                        
                                        <input type="submit" value={'Enviar'}></input>
                                    </div>
                                </form>
                            </Offcanvas>
                            <a onClick={handleDeleteSpent} style={{marginLeft:"0rem", marginTop:"0rem"}}><img src={trashIcon}></img></a>
                    </div>
                ))
            ))}
        </div>
        </Fragment>
)}; 