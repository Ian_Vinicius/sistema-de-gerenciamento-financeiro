import React, { Fragment, useState } from "react";
import axios from "axios";
import './styles/UserRevenue.css';
import Offcanvas from "./Offcanvas";
import Formulario from "../../template/Formulario";

import pencilIcon from "../../../assets/img/pencil-fill.svg";
import trashIcon from "../../../assets/img/trash3-fill.svg";

export default function UserRevenue(props) {
    function dateFormat(data) {
        const dataObj = typeof data === 'string' ? new Date(data) : data;
      
        const dia = dataObj.getDate().toString().padStart(2, '0');
        const mes = (dataObj.getMonth() + 1).toString().padStart(2, '0');
        const ano = dataObj.getFullYear();
      
        return `${dia}/${mes}/${ano}`;
      }

      function formatarData(received) {
        const date = new Date(received)
        const dia = date.getDate().toString().padStart(2, '0');
        const mes = (date.getMonth() + 1).toString().padStart(2, '0');
        const ano = date.getFullYear();
        return `${ano}-${mes}-${dia}`;
      }      

      const [userID, setUserID] = useState('');
      const [description, setDescription] = useState('');
      const [type, setType] = useState('');
      const [value, setValue] = useState('');
      const [date, setDate] = useState('');

      const [selectedOption, setSelectedOption] = useState('');

      const [revenueDescription, setRevenueDescription] = useState('');

      const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
      };
    
      const handleValueChange = (event) => {
        setValue(event.target.value);
      };
    
      const handleDateChange = (event) => {
        setDate(event.target.value);
      };


      const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.post(`http://localhost:5000/revenue/update}`, { 
                userID: localStorage.getItem('userId'),
                description, 
                type: selectedOption, 
                value, 
                date, 
            });
            console.log(response.data);
            setUserID("");
            setDescription("");
            setType("");
            setValue("");
            setDate("");
        } catch (error) {
            console.log(error);
        };
      };
       
      const handleDeleteRevenue = async (event) => {
        event.preventDefault();
        try {
            const receita = props.revenueContent.users[0].receitas[0][0];

            const response = await axios.post(`http://localhost:5000/revenue/delete`, { 
                revenueDescription: receita,
            });
            console.log(response.data);
            setRevenueDescription("");
        } catch (error) {
            console.log(error);
        };
      };


    return (
        <Fragment>
        <div className="user-revenue-area">
        <h4 style={{fontWeight:"700"}}>Recebimentos</h4>
            {props.revenueContent.users.map((user, userIndex) => (
                user.receitas.map((receita, receitaIndex) => (
                    <div key={`user-${userIndex}-receita-${receitaIndex}`} className="d-revenue">
                        <div style={{marginTop:"0.9rem"}}>
                            <p className="revenue-name">{receita[0]}</p>
                            <p className="revenue-date" style={{fontSize: "0.9rem", marginBottom:"0rem"}}>{dateFormat(receita[2])}</p>
                        </div>
                        <div style={{marginTop:"0.9rem"}}>
                            <p className="revenue-value" style={{color: "#00c53b", fontWeight: "800", marginBottom: "0rem"}}>R${receita[1]}</p>
                            <p className="revenue-type" style={{fontSize: "0.9rem"}}>{receita[3]}</p>
                        </div>
                            <Offcanvas IMG={pencilIcon} style={{marginLeft:"0rem", marginTop:"0rem"}}>
                            {/* <Formulario URL={'revenue/update'} Titulo="Atualizar recebimento" style={{color:'#00c53b', fontWeight:"700"}} Option1="Salário" Option2="Auxílio" Option3="Empréstimo" Option4="Investimentos" Option5="Outros" description={receita[0]} value={receita[1]} date={receita[2]} selectedOption={receita[3]}  /> */}
                            
                            <h2 style={{color:'#00c53b', fontWeight:"700"}}>Atualizar: {receita[0]}</h2>
                            <p>Valor: {receita[1]}</p>
                            <p>Data: {receita[2]}</p>
                            <p>Tipo: {receita[3]}</p>

                            <form className="formulario" onSubmit={handleSubmit}>
                                    <div className='forms-area'>
                                        <label htmlFor="descricao">Título:</label>
                                        <input type="text" id="descricao" name="descricao" onChange={handleDescriptionChange}/>
                                        
                                        <label htmlFor="valor">Valor:</label>
                                        <input type="number" id="valor" name="valor" onChange={handleValueChange}/>
                                    </div>

                                    <div className='forms-area'>
                                        <label htmlFor="tipo">Tipo:</label>
                                        <select id="tipo" name="tipo" onChange={(e) => setSelectedOption(e.target.value)}>
                                        <option value={'Salário'}>{'Salário'}</option>
                                        <option value={'Auxílio'}>{'Auxílio'}</option>
                                        <option value={'Empréstimo'}>{'Empréstimo'}</option>
                                        <option value={'Investimentos'}>{'Investimentos'}</option>
                                        <option value={'Outros'}>{'Outros'}</option>
                                        </select>
                                    
                                        <label htmlFor="data">Data:</label>
                                        <input type="date" id="data" name="data" value={formatarData(receita[2])} onChange={handleDateChange}/>
                                        <input type="submit" value={'Enviar'}></input>
                                    </div>
                                </form>
                            </Offcanvas>
                            <a onClick={handleDeleteRevenue} style={{marginLeft:"0rem", marginTop:"0rem"}}><img src={trashIcon}></img></a>
                    </div>
                ))
            ))}
        </div>
    </Fragment>

    )
};