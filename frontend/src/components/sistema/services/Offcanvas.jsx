import React, { Fragment, useEffect, useState } from "react";
import './styles/Offcanvas.css';

export default function Offcanvas (props) {
    
    const [isActive, setIsActive] = useState(false);

    const toggleOffcanvas = () => {
        setIsActive(!isActive);
      };

    return (
        <Fragment>
                <span className="canvas-button-area" style={props.style} onClick={toggleOffcanvas}>
                    <img src={props.IMG} style={{width:"40px", height:"40px", marginTop:"1rem"}}></img>
                    <p>{props.ButtonName}</p>
                </span>
                <div className={`my-offcanvas ${isActive ? 'active' : ''}`}>
                    <button style={{marginLeft:"32rem", marginTop:"0rem", border:"none", backgroundColor:"transparent"}} onClick={toggleOffcanvas}>Fechar</button>
                    {props.children}
                </div>
        </Fragment>
    )
}