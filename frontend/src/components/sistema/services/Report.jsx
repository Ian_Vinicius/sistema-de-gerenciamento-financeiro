import React, { Fragment, useState, useEffect } from "react";
import './styles/Report.css';
import Plot from 'react-plotly.js';

const Report = props => {
    return(
        <Fragment>
            <div className="user-report-area">
                <h4 style={{fontWeight:"700"}}>Maiores gastos do mês</h4>
                <Plot
                data={[
                {type: 'pie', labels: ['Alimentação', 'Total'], values: [20, 80]},
                ]}
                layout={ {width: 650, height: 400, title: 'A Fancy Plot'} }
            />
            </div>
        </Fragment>
    )
};

export default Report;