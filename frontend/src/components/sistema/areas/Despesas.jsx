import React, { Fragment } from "react";
import './Despesas.css';

import HomeNav from "../../template/HomeNav";

const Despesas = () => {
    return(
        <Fragment>
            <div className="despesa-content">
                <div className="nav-zone">
                    <HomeNav />
                </div>
                <div className="despesas-section">
                    <div className="despesas-content">
                        <div className="despesas-title" style={{display:"flex", justifyContent:"space-between"}}>
                            <h1>Suas despesas</h1>
                            <i class="bi bi-plus-square-fill" style={{}}></i>
                        </div>
                        <div className="item-content" style={{display:"flex"}}>
                            <p className="despesa-item"></p>
                            <div className="despesa-funcoes" style={{boxShadow:"3px 3px 5px rgba(0, 0, 0, 0.3)", height:"2.5rem", width:"6.5rem", borderRadius:"6px", backgroundColor:"#272B35", margin:"0rem 1rem"}}>
                                <i class="bi bi-pencil-fill" style={{padding:"0rem 1.1rem"}}></i>
                                <i class="bi bi-trash3-fill"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
};

export default Despesas;