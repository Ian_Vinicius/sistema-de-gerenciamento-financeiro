import React, { Fragment } from "react";
import './Relatorio.css';

import HomeNav from "../../template/HomeNav";
import Plot from 'react-plotly.js';

export default function Relatorio () {
    return (
        <div className="relatorio-section">
            <div className="nav-zone">
                <HomeNav/>
            </div>
            <div className="relatorio-content">
                <div className="relatorio-box">
                <Plot
                data={[
                {
                    x: [1, 2, 3],
                    y: [2, 6, 3],
                    type: 'scatter',
                    mode: 'lines+markers',
                    marker: {color: 'red'},
                }
                ]}
                layout={ {width: 650, height: 400, title: 'A Fancy Plot'} }
            />
                </div>
                <div className="relatorio-box">
                <Plot
                data={[
                {type: 'bar', x: [1, 2, 3], y: [2, 5, 3]},
                ]}
                layout={ {width: 650, height: 400, title: 'A Fancy Plot'} }
            />
                </div>
                
                <div className="relatorio-box">
                <Plot
                data={[
                {type: 'pie', labels: ['Residential', 'Non-Residential', 'Utility'], values: [19, 26, 55]},
                ]}
                layout={ {width: 650, height: 400, title: 'A Fancy Plot'} }
            />
                </div>

                <div className="relatorio-box">
                <Plot
                data={[
                {
                    x: [1, 2, 3],
                    y: [2, 6, 3],
                    type: 'scatter',
                    mode: 'lines+markers',
                    marker: {color: 'red'},
                },
                {type: 'bar', x: [1, 2, 3], y: [2, 5, 3]},
                ]}
                layout={ {width: 650, height: 400, title: 'A Fancy Plot'} }
            />
                </div>
            </div>
        </div>
    )
}