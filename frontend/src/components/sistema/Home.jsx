import React, { Fragment, useState, useEffect } from "react";
import './Home.css';
import { useUserContext } from '../../UserContext';

import UserDash from "./services/UserDash";
import UserSpent from "./services/UserSpent";
import UserRevenue from "./services/UserRevenue";
import UserLimits from "./services/UserLimits";
import Report from "./services/Report";
import HomeNav from "../template/HomeNav";

const Home = (props) => {

    const userId = useUserContext();
    const [data, setData] = useState([{}])
    const [userID, setUserID] = useState('');

    useEffect(() => {    
        const storedUserId = localStorage.getItem('userId');
        
        if (!storedUserId) {
            localStorage.setItem('userId', userId.userId);
            setUserID(userId.userId);
        } else {
            setUserID(storedUserId);
        }
        fetch(`http://127.0.0.1:5000/users/${localStorage.getItem('userId')}`).then(
            response => response.json()
        ).then(
            data => {
                setData(data) 
                console.log(data)
            }
        )
    }, [])

    // const mapRevenue = (data) => {
    //     const mapedData = data.users.map(user => user.receitas)
    //     return mapedData
    // };

    // const mapSpent = (data) => {
    //     const mapedData = data.users.map(user => user.despesas)
    //     return mapedData
    // };

    return (
        <Fragment>
        {(typeof data.users === "undefined") ? (<p>Loading...</p>) : (data.users.map((user) => (
            <div className="home-content">
                
                <HomeNav></HomeNav>

                <div className="main-home">
                    <div className="user-dash">
                        <UserDash Nome={user.nome} Saldo={user.saldo} Receita={data.users[0].receitas} Despesa={data.users[0].despesas}/>
                    </div>

                    <div className="user-options">
                        <div className="user-pay">
                            <UserSpent spentContent={data} />
                        </div>

                        <div className="user-receive">
                            <UserRevenue revenueContent={data}/>
                        </div>

                        <div className="user-account">
                            <Report /> 
                        </div>  

                        <div className="user-limits">
                            <UserLimits />
                        </div>
                    </div>
                </div>
            </div>
        ))
    )}
    </Fragment>
    )
};

export default Home;