from flask import Flask, session, redirect, url_for
from flask_cors import CORS, cross_origin
from flask_session import Session

from database import *
from gerencial import *
from exportacao import *

app = Flask(__name__)
CORS(app, supports_credentials=True)
app.config['CORS_HEADERS'] = 'Content-Type'
app.secret_key = 'be7fc5b6-259f-11ee-b43a-11b68e95b83e'
app.config['SESSION_TYPE'] = 'filesystem'

@app.route('/register', methods = ["GET", "POST"])
def addUser():
    try:
        getUserInformations()
        return jsonify({"success": True})
    except:
        return jsonify({"failed": False})


@app.route('/revenue', methods = ["GET", "POST"])
def setRevenue():
    try:
        setReceita()
        return jsonify({"success": True})
    except:
        return jsonify({"failed": False})
    
@app.route('/spent', methods = ["GET", "POST"])
def setSpent():
    try:
        setDespesa()
        return jsonify({"success": True})
    except:
        return jsonify({"failed": False})

@app.route('/revenue/update', methods = ["POST"])
def updateRevenue():
    try:
        updateReceita()
        return jsonify({"success": True})
    except:
        return jsonify({"failed": False})

# @app.route('/spent/update', methods = ["GET", "POST"])
# def updateSpent():
#     try:
#         updateDespesa()
#         return jsonify({"success": True})
#     except:
#         return jsonify({"failed": False})

@app.route('/revenue/delete', methods = ["POST"])
def deleteRevenue():
    try:
        deleteReceita()
        return jsonify({"success": True})
    except:
        return jsonify({"failed": False})

@app.route('/users/<string:user_id>', methods = ["GET", "POST"])
def user_profile(user_id):
    
    userInformations = getUserById(user_id)
    accountInformations = getAccountInformation(user_id)

    username = userInformations[1]
    usermail = userInformations[2]

    print(user_id, username, usermail)
    print(accountInformations)

    dados = {
        "users": [{
            "id": user_id,
            "nome": username,
            "email": usermail,
            "saldo": "revenueValue",
            "receitas": accountInformations[2],
            "despesas": accountInformations[3],
            "limites": 0
        }]
    }
    return dados

@app.route('/login', methods = ["POST"])
def login():
    try:
        verifyUserAccount = logUser()
        print(verifyUserAccount)

        idusuario = verifyUserAccount[1][1]

        if verifyUserAccount[0] == True:
            return jsonify({"success": True, "idusuario": idusuario})
        else:
            return jsonify({"failed": False})
    except:
        return jsonify({"Error": True})


if __name__ == "__main__":
    app.run(debug=True)