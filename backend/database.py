#Importando MySQL Connector
import mysql.connector
from mysql.connector import errorcode

#Funções SQL
# def start_connection():
#     connection = None
#     try:
#         connection = mysql.connector.connect(host='localhost', user='root', password='alunoifro', database='monetis')
#         print('\033[1;49;32mConexão estabelecida.\033[m')

#     except mysql.connector.Error as error:
#         print('\033[1;49;91mO database não existe ou não foi encontrado: {}\033[m'.format(error))

#     return connection
def start_connection():
    connection = None
    try:
        connection = mysql.connector.connect(host='localhost', user='root', password='sql060211nerd', database='monetis')
        print('\033[1;49;32mConexão estabelecida.\033[m')

    except mysql.connector.Error as error:
        print('\033[1;49;91mO database não existe ou não foi encontrado: {}\033[m'.format(error))

    return connection

def search_data(connection, SQL_search_data):
    data = None
    try:
        cursor = connection.cursor()
        cursor.execute(SQL_search_data)
        data = cursor.fetchall()
        print('\033[1;49;32mBusca de dados bem sucedida.\033[m')

    except mysql.connector.Error as error:
        print('\033[1;49;91mBusca de dados mal sucedida: {}\033[m'.format(error))
            
    return data

def insert_data(connection, SQL_insert_data, data):
    try:
        cursor = connection.cursor()
        cursor.execute(SQL_insert_data, data)
        connection.commit()
        connection.close()
        print('\033[1;49;32mInserção de dados bem sucedida.\033[m')
    except mysql.connector.Error as error:
	    print('\033[1;49;91mInserção de dados mal sucedida: {}\033[m'.format(error))

def alter_data(connection, SQL_alter_data):
    try:
        cursor = connection.cursor()
        cursor.execute(SQL_alter_data)
        connection.commit()
        print('\033[1;49;32mInserção de dados bem sucedida.\033[m')
    except mysql.connector.Error as error:
	    print('\033[1;49;91mInserção de dados mal sucedida.\033[m')
         
def delete_data(connection, SQL_delete_data):
    try:
        cursor = connection.cursor()
        cursor.execute(SQL_delete_data)
        connection.commit()
        connection.close()
        print(SQL_delete_data)
        print('\033[1;49;32mDados deletados.\033[m')
    
    except mysql.connector.Error as error:
	    print('\033[1;49;91mNão foi possível deletar os dados..\033[m')