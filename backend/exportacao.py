from flask import Flask, request, session,jsonify, make_response
from database import search_data, insert_data, alter_data, delete_data, start_connection

def setReceita():
    
    idUsuario = request.json.get("userID")
    description = request.json.get("description")
    type = request.json.get("type")
    value = request.json.get("value")
    date = request.json.get("date")

    print(description, type, value, date, idUsuario)

    connection = start_connection()
    SQL_insert_data = "INSERT INTO recebimentos(nome_recebimento, valor_recebimento, data_recebimento, tipo_recebimento, idrecebimento) VALUES (%s, %s, %s, %s, %s)"
    data = (description, value, date, type, idUsuario)
    print(SQL_insert_data)
    insert_data(connection, SQL_insert_data, data)

def setDespesa():

    idUsuario = request.json.get("userID")
    description = request.json.get("description")
    type = request.json.get("type")
    value = request.json.get("value")
    date = request.json.get("date")

    connection = start_connection()
    SQL_insert_data = "INSERT INTO gastos(nome_gasto, valor_gasto, data_gasto, tipo_gasto, idgastos) VALUES (%s, %s, %s, %s, %s)"
    data = (description, value, date, type, idUsuario)
    insert_data(connection, SQL_insert_data, data)

def setLimite():
    
    idUsuario = request.json.get("userID")
    categoria = request.json.get("description")
    valor_estabelecido = request.json.get("type")
    valor_gasto = request.json.get("value")
    valor_restante = request.json.get("date")

    connection = start_connection()
    SQL_insert_data = "INSERT INTO gastos(nome_categoria, valor_estabelecido, valor_gasto, valor_restante, idlimites) VALUES (%s, %s, %s, %s, %s)"
    data = (categoria, valor_estabelecido, valor_gasto, valor_restante, idUsuario)
    insert_data(connection, SQL_insert_data, data)

def updateReceita():
    idUsuario = request.json.get("userID")
    description = request.json.get("description")
    type = request.json.get("type")
    value = request.json.get("value")
    date = request.json.get("date")

    print(description, type, value, date, idUsuario)

    connection = start_connection()
    SQL_insert_data = "UPDATE recebimentos SET nome_recebimento = (%s), valor_recebimento = (%s), data_recebimento = (%s), tipo_recebimento = (%s), idrecebimento = (%s) WHERE nome_gasto = (%s)"

    data = (description, value, date, type, idUsuario, description)
    alter_data(connection, SQL_insert_data, data)

def updateDespesa():
    idUsuario = request.json.get("userID")
    description = request.json.get("description")
    type = request.json.get("type")
    value = request.json.get("value")
    date = request.json.get("date")

    print(description, type, value, date, idUsuario)

    connection = start_connection()
    SQL_insert_data = "UPDATE recebimentos SET nome_recebimento = (%s), valor_recebimento = (%s), data_recebimento = (%s), tipo_recebimento = (%s), idrecebimento = (%s) WHERE nome_gasto = (%s)"

    data = (description, value, date, type, idUsuario, description)
    alter_data(connection, SQL_insert_data, data)


def deleteReceita():
    descripition = request.json.get("revenueDescription")

    print(descripition)

    connection = start_connection()
    SQL_delete_data = "DELETE FROM recebimentos WHERE nome_recebimento = %s"
    data = (descripition)

    print(SQL_delete_data)
    delete_data(connection, SQL_delete_data, data)