from flask import Flask, request, jsonify, make_response
from database import search_data, insert_data, start_connection

from server import app
import uuid

#Inserindo usuário no sistema
def getUserInformations():
    #Recebendo informações do frontend
    username = request.json.get("username")
    email = request.json.get("email")
    password = request.json.get("password")

    #Criando ID do usuário 
    useruuid = uuid.uuid1()
    stringfyID = str(useruuid)
    splitID = stringfyID.split("-")
    new_id = splitID[0] + splitID[1] + splitID[3]

    connection = start_connection()
    SQL_insert_data = "INSERT INTO usuario(idusuario, nome_usuario, email_usuario, senha_usuario) VALUES (%s, %s, %s, %s)"
    data = (new_id, username, email, password)
    insert_data(connection, SQL_insert_data, data)

def logUser():
    try:
        LoginEmail = request.json.get("logEmail")
        LoginPassword = request.json.get("logPassword")
        str(LoginEmail)
        str(LoginPassword)
        connection = start_connection()

        SQL_search_data = (f'SELECT email_usuario, senha_usuario FROM usuario WHERE email_usuario + senha_usuario IN ("{LoginEmail}", "{LoginPassword}")')
        SQL_auxiliar = search_data(connection, SQL_search_data)

        if SQL_auxiliar != None:
            isUserRegistered  = True
            userInformations = getUserbyEmail(LoginEmail)
            updatedUserInformations = [isUserRegistered, userInformations]
            return updatedUserInformations
        
    except:
        isUserRegistered = False


def getUserbyEmail(loginEmail):
    connection = start_connection()
    SQL_search_data = (f'SELECT idusuario, nome_usuario FROM usuario WHERE email_usuario = ("{loginEmail}")')
    SQL_auxiliar = search_data(connection, SQL_search_data)
    
    GetUserInformations = SQL_auxiliar[0]
    getUserID = GetUserInformations[0]
    getUsername = GetUserInformations[1]
    getUserEmail = loginEmail

    if SQL_auxiliar != None:
        userInformations = True
    else:
        userInformations = False
    
    return [userInformations, getUserID, getUsername, getUserEmail]


def getUserById(idusuario):
    connection = start_connection()
    SQL_search_data = (f'SELECT nome_usuario, email_usuario FROM usuario WHERE idusuario = ("{idusuario}")')
    SQL_auxiliar = search_data(connection, SQL_search_data)
    
    GetUserInformations = SQL_auxiliar[0]
    getUsername = GetUserInformations[0]
    getUserEmail = GetUserInformations[1]
    
    if SQL_auxiliar != None:
        userInformations = True
    else:
        userInformations = False
    
    return [userInformations, getUsername, getUserEmail]

def getAccountInformation(idusuario):

    revenueInformations = []
    spentInformations = []

    connection = start_connection()
    SQL_search_data = (f'SELECT nome_recebimento, valor_recebimento, data_recebimento, tipo_recebimento FROM recebimentos WHERE idrecebimento = ("{idusuario}")')
    SQL_auxiliar_revenue = search_data(connection, SQL_search_data)
 
    if SQL_auxiliar_revenue != None:
        isThereRevenues = True
        for revenues_index in SQL_auxiliar_revenue:
            revenue_information = []
            for revenue_index in revenues_index:
                revenue_information.append(revenue_index)
                # revenue_information = [getRevenueDescription, getRevenueValue, getRevenueDate, getRevenueType]
            revenueInformations.append(revenue_information)
    else:
        isThereRevenues = False


    #---------------------------------------#
    connection = start_connection()
    SQL_search_data = (f'SELECT nome_gasto, valor_gasto, data_gasto, tipo_gasto FROM gastos WHERE idgastos = ("{idusuario}")')
    SQL_auxiliar_spent = search_data(connection, SQL_search_data)

    if SQL_auxiliar_revenue != None:
        isThereSpents = True
        for spents_index in SQL_auxiliar_spent:
            spent_information = []
            for spent_index in spents_index:
                spent_information.append(spent_index)
                # spent_information = [getSpentDescription, getSpentValue, getSpentDate, getSpentType]
            spentInformations.append(spent_information)
    else:
        isThereSpents = False
    
    return [isThereRevenues, isThereSpents, revenueInformations, spentInformations]


