# Monetis - Sistema Gerenciador Financeiro

O Monetis é um sistema web de gerenciamento financeiro pessoal projetado para ajudar seus usuários a organizar e controlar suas finanças de maneira eficiente. O sistema tem como objetivo principal fornecer uma plataforma de gerenciamento financeiro que seja fácil de usar e acessível a partir de qualquer dispositivo, seja desktop ou mobile.


![Logo](https://i.pinimg.com/originals/dd/b8/34/ddb83413ffd9b5c0fc477a4a6e155fd9.png)


## Sistematização
A falta de planejamento financeiro é um problema recorrente e pouco debatido, que ocorre quando uma pessoa ou empresa não tem um plano financeiro bem definido, o que pode levar a gastos excessivos, dívidas e problemas financeiros.

O sistema é projetado para registrar, gerenciar fluxos de gastos, gerar relatórios financeiros e fornecer insights para tomada de decisões financeiras. Isso pode incluir o uso de interfaces de usuário intuitivas, recursos de automação para simplificar tarefas de gerenciamento financeiro e a integração com outras ferramentas.


## Funcionalidades

- Suas receitas
- Suas despesas
- Limite de gastos
- Relatórios mensais


## Autores

- [@iansantos](https://www.github.com/NerdAleatorio)
- [@isabellaqueiroz](https://www.instagram.com/isabellac_queiroz/)
- [@samuelfreitas](https://www.instagram.com/muelxt/)
- [@thiagotome](https://www.instagram.com/thiago__thom3/)
- [@yurig](https://github.com/ygbriel)


## Stack utilizada

**Front-end:** JavaScript, Plotly, Figma, VS Code

**Back-end:** Python, Flask_server


## Feedback

Se você tiver algum feedback, por favor nos deixe saber por meio de suporte.pvortex@gmail.com


## Referências

- [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
- [Awesome README](https://github.com/matiassingers/awesome-readme)
- [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)
